package com.aya.guava;

import com.google.common.collect.Lists;
import org.junit.Test;

import java.util.List;

/**
 * @author liushang@zsyjr.com
 * @version 1.0
 * @description 描述信息
 * @date 2020/1/10
 **/
public class ListsTest {
    @Test
    public void testListPartition(){
        List<String> emptyList = Lists.newArrayList();
        List<List<String>> partition = Lists.partition(emptyList, 100);
        for (List<String> list : partition) {
            for (String s : list) {
                System.out.println(s);
            }
        }
    }

    @Test
    public void testEmptyLists(){
        String nullStr = null;
        List<String> emptyList = Lists.newArrayList(nullStr);
        for (String s : emptyList) {
            System.out.println(s);
        }


    }
}
