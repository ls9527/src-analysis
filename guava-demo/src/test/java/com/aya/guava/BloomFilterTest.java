package com.aya.guava;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnel;
import com.google.common.hash.Funnels;
import org.junit.Test;

import java.nio.charset.Charset;


public class BloomFilterTest {

    @Test
    public void testBloom() {

        final Funnel<CharSequence> charSequenceFunnel = Funnels.stringFunnel(Charset.defaultCharset());
        final BloomFilter<CharSequence> charSequenceBloomFilter = BloomFilter.create(charSequenceFunnel, 10);

        charSequenceBloomFilter.put("AABB");
        charSequenceBloomFilter.put("AABC");
        charSequenceBloomFilter.put("AABD");

        System.out.println(charSequenceBloomFilter.mightContain("AABB"));
        System.out.println(charSequenceBloomFilter.mightContain("ABAB"));
    }
}
