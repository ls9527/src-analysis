package com.aya;

import org.junit.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class HashMapTest {
    @Test
    public void testConstructorA() {
        HashMap<String, String> hashMap = new HashMap<>();
    }

    @Test
    public void testConstructorB() {
        HashMap<Integer, Object> hashMap = new HashMap<>(2);
    }

    @Test
    public void testConstructorC() {
        HashMap<Integer, Object> hashMap = new HashMap<>(7, 0.75F);
    }

    @Test
    public void testConstructorD() {
        HashMap<Integer, Object> hashMap = new HashMap<>();
        hashMap.put(1, "a");

        HashMap<Integer, Object> hashMapB = new HashMap<>(hashMap);
    }


    @Test
    public void testResizeB() {
        HashMap<Integer, Object> hashMap = new HashMap<>(8, 0.3F);
        hashMap.put(1,"");
        hashMap.put(2,"");
        hashMap.put(3,"");
    }

    @Test
    public void testResizeC() {
        HashMap<Integer, Object> hashMap = new HashMap<>(16, 0.3F);
        hashMap.put(1,"");//第一次扩容
        hashMap.put(2,"");
        hashMap.put(24,"");
        hashMap.put(25,"");
        hashMap.put(26,"");//第二次扩容

    }

    /**
     * treeify 树化测试
     */
    @Test
    public void testPut() {
        HashMap<HashCollisionModel, Object> hashMap = new HashMap<>();
        hashMap.put(new HashCollisionModel(1), "Edgar");//无hash碰撞
        hashMap.put(new HashCollisionModel(3), "Helen");//加入链表末尾
        hashMap.put(new HashCollisionModel(5), "William");//加入链表末尾
        hashMap.put(new HashCollisionModel(7), "Jim");//加入链表末尾
        hashMap.put(new HashCollisionModel(9), "Kenneth");//加入链表末尾
        hashMap.put(new HashCollisionModel(11), "Heathcliff");//加入链表末尾
        hashMap.put(new HashCollisionModel(13), "Earnshaw");//加入链表末尾
        hashMap.put(new HashCollisionModel(15), "Catherine");//加入链表末尾
        hashMap.put(new HashCollisionModel(17), "Joseph");//加入链表末尾,扩充容量到32
        hashMap.put(new HashCollisionModel(19), "Linton");//加入链表末尾,扩充容量到64
        hashMap.put(new HashCollisionModel(21), "Lockwood");//树化 替换
        hashMap.put(new HashCollisionModel(23), "Roth");//树化 添加
        hashMap.put(new HashCollisionModel(25), "Jack");//树化 添加
    }

    /**
     * hash 碰撞  结果被替换 1
     */
    @Test
    public void testHashReplaceA() {
        HashMap<HashCollisionModel, Object> hashMap = new HashMap<>();
        hashMap.put(new HashCollisionModel(3), "Jim");//未hash碰撞
        hashMap.put(new HashCollisionModel(3), "William");//不是最后一个节点 && 相等
    }

    /**
     * hash 碰撞  结果被替换 2
     */
    @Test
    public void testHashReplaceB() {
        HashMap<HashCollisionModel, Object> hashMap = new HashMap<>();
        hashMap.putIfAbsent(new HashCollisionModel(3), null);//未hash碰撞
        hashMap.putIfAbsent(new HashCollisionModel(3), "William");//不是最后一个节点 && 相等
    }

    /**
     * hash 碰撞  结果不被替换,使用原值
     */
    @Test
    public void testHashNotReplace() {
        HashMap<HashCollisionModel, Object> hashMap = new HashMap<>();
        hashMap.putIfAbsent(new HashCollisionModel(3), "Jim");//未hash碰撞
        hashMap.putIfAbsent(new HashCollisionModel(3), "William");//不是最后一个节点 && 相等
    }

    @Test
    public void testEqualsFirst() {
        HashMap<HashCollisionModel, Object> hashMap = new HashMap<>();
        hashMap.put(new HashCollisionModel(3), "Poal");
        hashMap.put(new HashCollisionModel(3), "Helen");
    }


    @Test
    public void testEqualsEach() {
        HashMap<HashCollisionModel, Object> hashMap = new HashMap<>();
        hashMap.put(new HashCollisionModel(1), "William");
        hashMap.put(new HashCollisionModel(3), "Poal");
        hashMap.put(new HashCollisionModel(3), "Helen");
    }


    @Test
    public void testGet() {
        HashMap<Integer, Object> hashMap = new HashMap<>();
        hashMap.putIfAbsent(3, "data");
        hashMap.get(3);
    }

    @Test
    public void testPutLinkedList() {
        HashMap<HashCollisionModel, Object> hashMap = new HashMap<>();
        hashMap.put(new HashCollisionModel(1), "Edgar");//无hash碰撞
        hashMap.put(new HashCollisionModel(3), "Helen");//加入链表末尾
        hashMap.put(new HashCollisionModel(5), "William");//加入链表末尾
    }

    @Test
    public void testGetTree() {
        HashMap<HashCollisionModel, Object> hashMap = new HashMap<>();
        hashMap.put(new HashCollisionModel(1), "Edgar");//无hash碰撞
        hashMap.put(new HashCollisionModel(3), "Helen");//加入链表末尾
        hashMap.put(new HashCollisionModel(5), "William");//加入链表末尾
        hashMap.put(new HashCollisionModel(7), "Jim");//加入链表末尾
        hashMap.put(new HashCollisionModel(9), "Kenneth");//加入链表末尾
        hashMap.put(new HashCollisionModel(11), "Heathcliff");//加入链表末尾
        hashMap.put(new HashCollisionModel(13), "Earnshaw");//加入链表末尾
        hashMap.put(new HashCollisionModel(15), "Catherine");//加入链表末尾
        hashMap.put(new HashCollisionModel(17), "Joseph");//加入链表末尾,扩充容量到32
        hashMap.put(new HashCollisionModel(19), "Linton");//加入链表末尾,扩充容量到64
        hashMap.put(new HashCollisionModel(21), "Lockwood");//树化 替换
        hashMap.put(new HashCollisionModel(23), "Roth");//树化 添加
        hashMap.put(new HashCollisionModel(25), "Jack");//树化 添加
        hashMap.remove(new HashCollisionModel(1));//树化 添加
        hashMap.get(new HashCollisionModel(15));//树化获取
    }


    @Test
    public void testThreadSafe() throws InterruptedException {
        String EMPTY_VALUE ="";
        HashMap<Integer, Object> hashMap = new HashMap<>();
        hashMap.put(1, EMPTY_VALUE);
        Thread threadA = new Thread(() -> {
            Iterator<Integer> iterator = hashMap.keySet().iterator();
            System.out.println("进入 iterator 临界区");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while (iterator.hasNext()){
                Integer key = iterator.next();
                System.out.println(key);
            }

            System.out.println("并发判断");
        });

        Thread threadB = new Thread(() -> {
            System.out.println("并发插入节点3");
            hashMap.put(3,EMPTY_VALUE);
        });
        threadA.start();
        Thread.sleep(1000);
        threadB.start();

        threadA.join();
        threadB.join();
    }

}
