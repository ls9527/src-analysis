package com.aya;

import org.junit.Test;
import sun.misc.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

public class ArrayListTest {

    @Test
    public void testInitialA(){
        ArrayList<String> arrayList = new ArrayList<>();
    }


    @Test
    public void testInitialB(){
        ArrayList<String> arrayList = new ArrayList<>(8);
    }

    @Test
    public void testInitialC(){

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        ArrayList<String> arrayListB = new ArrayList<>(arrayList);

    }

    // 测试添加

    @Test
    public void testAdd(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("a");
    }

    // 测试添加

    @Test
    public void testRemove(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        arrayList.remove(1);
        System.out.println(arrayList);
    }

    // 测试存在

    @Test
    public void testContains(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        System.out.println(arrayList.contains("a"));
    }
    // 测试获取
    @Test
    public void testGet(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        String s = arrayList.get(0);

    }

    @Test
    public void testSerialize() throws IOException {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("a");
        arrayList.add("a");
        arrayList.remove(0);
        arrayList.add("a");
        String s = arrayList.get(0);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(arrayList);
        objectOutputStream.close();

        byte[] bytes = byteArrayOutputStream.toByteArray();
        System.out.println(bytes);
    }




}
