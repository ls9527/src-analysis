package com.aya;

import org.junit.Test;

import java.util.LinkedList;

public class LinkedListTest {
    @Test
    public void testHashSet(){
        LinkedList linkedList = new LinkedList();
        linkedList.add("1");
        linkedList.add("2");
        linkedList.add("3");
        linkedList.add("4");
        linkedList.add("5");
        linkedList.add("6");
        linkedList.add(4,"10");

        linkedList.push("100");
        linkedList.pop();

        linkedList.offer("1000");
        linkedList.offerFirst("1000");
        linkedList.offerLast("1000");
        System.out.println(linkedList);
    }
}
