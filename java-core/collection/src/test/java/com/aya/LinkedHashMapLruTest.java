package com.aya;

import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

public class LinkedHashMapLruTest {
    @Test
    public void testLru() {
        final int limit = 5;
        LinkedHashMap<String, String> lru = new LinkedHashMap<String, String>() {
            @Override
            protected boolean removeEldestEntry(Map.Entry<String, String> eldest) {
                return size() > limit;
            }
        };

        lru.put("1","1");
        lru.put("2","1");
        lru.put("3","1");
        lru.put("1","1");
        lru.put("2","1");
        System.out.println(lru);
        lru.put("4","1");
        lru.put("5","1");
        System.out.println(lru);
        lru.put("6","1");
        System.out.println(lru);
    }
}
