package com.aya;

import org.junit.Test;

import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class HashMapConcurrentPut {


    private static Map<String, String> map = new MyHashMap<>(8);

    public static void main(String[] args) throws InterruptedException {

        map.put("1", "EMPTY");
        map.put("2", "EMPTY");
        map.put("3", "EMPTY");
        map.put("4", "EMPTY");
        map.put("5", "EMPTY");

        CountDownLatch countDownLatch = new CountDownLatch(1);
        Thread threadA = new Thread(() -> {
            try {
                countDownLatch.await();
                map.put("9", "a");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "hashmap-concurrent-a");


        Thread threadB = new Thread(() -> {
            try {
                countDownLatch.await();
                map.put("10", "b");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "hashmap-concurrent-b");

        threadB.start();
        threadA.start();

        TimeUnit.SECONDS.sleep(2);
        countDownLatch.countDown();
        TimeUnit.SECONDS.sleep(1);
        String s = map.get("9");
        System.out.println(s);

//        TimeUnit.HOURS.sleep(1);

    }

    // 找到 11 在256 范围内的hash:  32
    // 找到 99 在256 范围内的hash:  32
    // 那么字符串 11 和 99 在256范围的hash是相等的
    @Test
    public void testFindHash() {
        int maxLength = 0x8;
        System.out.println(maxLength);
        for (int i = 0; i < 100; i++) {
            int i1 = (i + "").hashCode() & maxLength;
            System.out.println(i + "," + i1);
        }
    }
}
