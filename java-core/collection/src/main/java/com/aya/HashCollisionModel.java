package com.aya;


public class HashCollisionModel {
    private Integer number;

    public Integer getNumber() {
        return number;
    }

    public HashCollisionModel(Integer number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){ return true;}
        if(o instanceof HashCollisionModel){
            HashCollisionModel that = (HashCollisionModel) o;
            return that.number .equals( number);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return number%2+1;
    }
}
