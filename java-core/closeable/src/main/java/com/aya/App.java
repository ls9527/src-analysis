package com.aya;

public class App {
    public static void main(String[] args) {
        try (MyCloseable myCloseable = new MyCloseable();MyCloseable myCloseableA = new MyCloseable()) {
            System.out.println("完全没有异常");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("用户异常处理");
        }
    }
}
