package com.aya;

import java.io.Closeable;
import java.io.IOException;

public class MyCloseable implements Closeable {
    @Override
    public void close() throws IOException {
        System.out.println("关闭资源");
    }
}
