package com.aya.visitor;

public class PrintVisitor implements Visitor {
    private int depth = 0;

    @Override
    public void visitor(Menu menu) {
        print("菜单名称:" + menu.getName());
        depth++;
        for (Component component : menu.getComponents()) {
            component.accept(this);
        }
        depth--;
    }

    @Override
    public void visitor(Item menu) {
        print("项目名称:" + menu.getName());
    }

    private void print(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < depth; i++) {
            stringBuilder.append("\t");
        }
        stringBuilder.append(str);
        System.out.println(stringBuilder);
    }
}
