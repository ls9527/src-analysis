package com.aya.visitor;

public class Item extends Component {
    @Override
    public void accept(Visitor visitor) {
        visitor.visitor(this);
    }

    public Item() {
    }

    public Item(String name) {
        super();
        super.setName(name);
    }

}
