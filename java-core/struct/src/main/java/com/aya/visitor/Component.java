package com.aya.visitor;

public abstract class Component {

    private String name;
    private String description;

    public abstract void accept(Visitor visitor);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void addComponent(Component component) {
        throw new IllegalStateException("无法添加组件");
    }

    public void removeComponent(Component component) {
        throw new IllegalStateException("无法删除组件");
    }
}
