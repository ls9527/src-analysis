package com.aya.visitor;

import java.util.ArrayList;
import java.util.List;

public class Menu extends Component {
    List<Component> components = new ArrayList<>();

    public Menu() {
    }

    public Menu(String name) {
        super();
        super.setName(name);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitor(this);
    }

    @Override
    public void addComponent(Component component) {
        components.add(component);
    }

    @Override
    public void removeComponent(Component component) {
        components.remove(component);
    }

    public List<Component> getComponents() {
        return components;
    }
}
