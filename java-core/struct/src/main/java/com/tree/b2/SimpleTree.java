package com.tree.b2;

public class SimpleTree {
    SimpleTree left;
    SimpleTree right;

    Object src;

    public SimpleTree(Object src) {
        this.src = src;
    }

    /**
     * 前置二叉树
     */
    public void frontPrint(){
        if(left != null){
            left.frontPrint();
        }
        System.out.println(src);
        if(right != null){
            right.frontPrint();
        }
    }

    public void setLeft(SimpleTree left) {
        this.left = left;
    }

    public void setRight(SimpleTree right) {
        this.right = right;
    }

}
