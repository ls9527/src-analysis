package com.aya.visitor;

import org.junit.Test;

public class VisitorTest {
    @Test
    public void testVisitor() {
        Menu root = new Menu("主菜单");
        root.addComponent(new Item("香辣鸭脖"));
        root.addComponent(new Item("剁椒鱼头"));

        Menu childMenu = new Menu("饮料栏目");
        childMenu.addComponent(new Item("雪花纯生"));
        childMenu.addComponent(new Item("百威"));

        root.addComponent(childMenu);

        root.addComponent(new Item("猪肚鸡"));


        root.accept(new PrintVisitor());

    }
}
