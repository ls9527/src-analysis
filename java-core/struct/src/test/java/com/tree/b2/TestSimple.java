package com.tree.b2;

import org.junit.Test;

public class TestSimple {
    @Test
    public void testFrontPrint(){
        SimpleTree simpleTree = new SimpleTree(5);

        simpleTree.left = new SimpleTree(3);
        simpleTree.right = new SimpleTree(8);

        simpleTree.left.left =  new SimpleTree(1);
        simpleTree.left.right =  new SimpleTree(4);

        simpleTree.right.left =  new SimpleTree(6);
        simpleTree.right.right =  new SimpleTree(9);

        simpleTree.frontPrint();
    }

}
