package com.aya;

import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.*;

public class ThreadUseTest {
Logger logger = Logger.getLogger(ThreadUseTest.class);
    /**
     * 同步等待所有任务处理完成方案
     * @throws InterruptedException
     */
    @Test
    public void testSync() throws InterruptedException {
        final  int size = Integer.MAX_VALUE;
        final ThreadPoolExecutor executorService =  new ThreadPoolExecutor(5, 8,
                500000L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(5),new ThreadPoolExecutor.CallerRunsPolicy());
        for(int i=0;i<size;i++){


            for (int i1 = 1; i1 <= 13; i1++) {

//                int size1 = executorService.getQueue().size();
//                if(size1==5){
//                    logger.info("MAIN,activeCount:"+executorService.getActiveCount()
//                            + ",taskCount:"+executorService.getTaskCount()
//                            + ",completedTaskCount:"+executorService.getCompletedTaskCount()
//                            + ",corePool:"+executorService.getCorePoolSize()
//                            + ",maxPool:"+executorService.getMaximumPoolSize()
//                            + ",queueSize:"+ size1);
//                }

                int f = i1;
                executorService.execute(new Runnable() {
                    int index = f;
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(1000);

                            logger.info("进入线程,activeCount:"+executorService.getActiveCount()
                                    + ",taskCount:"+executorService.getTaskCount()
                                    + ",completedTaskCount:"+executorService.getCompletedTaskCount()
                                    + ",corePool:"+executorService.getCorePoolSize()
                                    + ",maxPool:"+executorService.getMaximumPoolSize()
                                    + ",queueSize:"+ executorService.getQueue().size()
                                    + ",index:"+ index
                            );

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            Thread.sleep(10000);

        }

        Thread.sleep(5000000);
    }

}
