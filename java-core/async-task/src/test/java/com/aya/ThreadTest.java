package com.aya;

import org.junit.Test;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

public class ThreadTest {
    final  int SIZE = 200;
    /**
     * 简单耗时任务模拟
     */
    @Test
    public void testSimple(){
        Random random = new Random();
        Date start = new Date();
        final  int size = 200;
        for(int i=1;i<size;i++){
            try {
                Thread.sleep(10+random.nextInt(20));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Date end = new Date();
        System.out.println(end.getTime()-start.getTime());
    }

    /**
     * 同步等待所有任务处理完成方案
     * @throws InterruptedException
     */
    @Test
    public void testSync() throws InterruptedException {
        final  int size = 200;
        Random random = new Random();
        final CountDownLatch countDownLatch = new CountDownLatch(size);
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        Date start = new Date();
        int sum = 0;
        for(int i=0;i<size;i++){

                executorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(10+random.nextInt(20));
                            countDownLatch.countDown();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });

        }
        countDownLatch.await();
        Date end = new Date();
        System.out.println(end.getTime()-start.getTime());
    }

    /**
     * 异步回调方案
     * @throws InterruptedException
     */
    @Test
    public void testReturnAsync() throws InterruptedException {
        final  int size = 200;
        Random random = new Random();
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Date start = new Date();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Date end = new Date();
                System.out.println(end.getTime()-start.getTime());
            }
        };
        AtomicInteger atomicInteger = new AtomicInteger(size);
        for(int i=0;i<size;i++){

            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(10+random.nextInt(20));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(atomicInteger.decrementAndGet()==0){
                        runnable.run();
                    }
                }
            });

        }
        System.out.println("等待子线程的结果");
        Thread.sleep(2000);
    }


}
