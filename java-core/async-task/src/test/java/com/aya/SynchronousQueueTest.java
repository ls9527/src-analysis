package com.aya;

import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.concurrent.SynchronousQueue;

public class SynchronousQueueTest {
    Logger logger = Logger.getLogger(getClass());

    @Test
    public void testSyncQueue() throws InterruptedException {
        final SynchronousQueue<Integer> queue = new SynchronousQueue<Integer>();

        Thread putThread = new Thread(new Runnable() {
            @Override
            public void run() {
                logger.info("put thread start");
                try {
                    queue.put(1);
                } catch (InterruptedException e) {
                }
                logger.info("put thread end");
            }
        });

        Thread takeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                logger.info("take thread start");
                try {
                    logger.info("take from putThread: " + queue.take());
                } catch (InterruptedException e) {
                }
                logger.info("take thread end");
            }
        });

        putThread.start();
        Thread.sleep(1000);
        takeThread.start();

    }

    class PutThread extends Thread{
        final SynchronousQueue<Integer> queue;

        PutThread(SynchronousQueue<Integer> queue) {
            this.queue = queue;
            setDaemon(true);
        }


        @Override
        public void run() {
            logger.info("put thread start");
            try {
                queue.put(1);
            } catch (InterruptedException e) {
            }
            logger.info("put thread end");
        }
    }

    class TakeThread extends Thread{
        final SynchronousQueue<Integer> queue;

        TakeThread(SynchronousQueue<Integer> queue) {
            this.queue = queue;
            setDaemon(true);
        }


        @Override
        public void run() {
            logger.info("take thread start");
            try {
                logger.info("take from putThread: " + queue.take());
            } catch (InterruptedException e) {
            }
            logger.info("take thread end");
        }
    }

    @Test
    public void testSyncQueueSimpleA() throws InterruptedException {
        final SynchronousQueue<Integer> queue = new SynchronousQueue<Integer>();

        Thread putThread = new PutThread(queue);
        Thread takeThread = new TakeThread(queue);

        putThread.start();
        Thread.sleep(10000);
        takeThread.start();
        Thread.sleep(1000000);
    }

    @Test
    public void testSyncQueueSimpleB() throws InterruptedException {
        final SynchronousQueue<Integer> queue = new SynchronousQueue<Integer>();

        Thread putThread = new PutThread(queue);
        Thread takeThread = new TakeThread(queue);

        takeThread.start();
        Thread.sleep(30000);
        putThread.start();
        Thread.sleep(1000000);
    }
}
