package com.aya;

import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantReadWriteLockTest {

    @Test
    public void testConflict() throws InterruptedException {
        ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock(true);

        Thread threadA = new Thread(() -> {
            System.out.println("A 已进入");
            ReentrantReadWriteLock.ReadLock readLock = reentrantReadWriteLock.readLock();
            readLock.lock();
            try {
                System.out.println("A拿到了读锁");
                TimeUnit.DAYS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                readLock.unlock();
            }
            System.out.println("A 已结束");
        });

        Thread threadB = new Thread(() -> {
            try {
                threadA.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("B 已进入");
            ReentrantReadWriteLock.WriteLock writeLock = reentrantReadWriteLock.writeLock();
            writeLock.lock();
            try {
                System.out.println("B拿到了写锁");
            } finally {
                writeLock.unlock();
            }
            System.out.println("B 已结束");
        });

        threadA.start();
        threadB.start();
        Thread.sleep(2000);
        Thread threadC = new Thread(() -> {
            System.out.println("C 已进入");
            ReentrantReadWriteLock.ReadLock readLock = reentrantReadWriteLock.readLock();
            readLock.lock();
            try {
                System.out.println("C拿到了读锁");
            } finally {
                readLock.unlock();
            }
            System.out.println("C 已结束");
        });
        threadC.start();

        TimeUnit.DAYS.sleep(1);
    }
}
