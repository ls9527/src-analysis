package com.aya.count;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class CountTest {
    @Test
    public void testAwait() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        boolean await = countDownLatch.await(3L, TimeUnit.SECONDS);
        System.out.println(await);
    }

    @Test
    public void testAwaitTrue() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        countDownLatch.countDown();
        boolean await = countDownLatch.await(3L, TimeUnit.SECONDS);
        System.out.println(await);
    }
}
