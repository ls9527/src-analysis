package com.aya;

public class JvmStack {
    public static void main(String[] args) throws InterruptedException {
        methodA();
        Thread.sleep(100*1000);
    }

    public static void methodA() {
        int a = 1;
        methodB(a);
    }

    public static int methodB(int param) {
        return param;
    }
}
