package com.aya;

public class TryCatch {
    public static void main(String[] args) {
        try {
            doStart();
        } catch (RuntimeException e) {
            doException();
        }finally {
            doFinally();
        }
    }

    private static void doStart() {

    }

    private static void doException() {

    }

    private static void doFinally(){

    }
}
