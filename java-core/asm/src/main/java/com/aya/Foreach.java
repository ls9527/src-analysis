package com.aya;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class Foreach {

    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("Brown");

        for (String name:strings){
            System.out.println(name);
        }
    }
}
