package com.aya;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ForTest {

    public List<String> getList(){
        System.out.println("getList 被调用");
        ArrayList<String> strings = new ArrayList<String>();
        strings.add("Brown");
        strings.add("Carl");
        return strings;
    }

      @Test
    public void testForEach (){
        for (int i = 0;i<getList().size();i++){
            System.out.println(getList().get(i));
        }
    }

    @Test
    public void testForEachFix (){
        List<String> list = getList();
        for (int i = 0; i< list.size(); i++){
            System.out.println(list.get(i));
        }
    }






}
