package com.aya;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ForeachTest {
    @Test
    public void testterator (){
        List<String> strings = new ArrayList<>();
        strings.add("Brown");

        for (String name:strings){
            System.out.println(name);
        }
    }


    @Test
    public void testEachLessDate (){

        Date now = new Date();
        Date start = new Date(now.getTime());
        Date end = new Date(now.getTime());
        end.setDate(end.getDate()+2);;

        for (;start.before(end);start.setDate(start.getDate()+1)){
            System.out.println(start);
        }
    }

    @Test
    public void testEachLessEqualDate (){

        Date now = new Date();
        Date start = new Date(now.getTime());
        Date end = new Date(now.getTime());
        end.setDate(end.getDate()+2);

        for (;start.compareTo(end)<=0;start.setDate(start.getDate()+1)){
            System.out.println(start);
        }
    }





    public List<String> getList(){
        ArrayList<String> strings = new ArrayList<String>(){
            @Override
            public Iterator<String> iterator() {
                System.out.println("iterator 被调用");
                return super.iterator();
            }
        };

        strings.add("Brown");
        strings.add("Carl");
        return strings;
    }

    @Test
    public void testForEach (){
        for (String name:getList()){
            System.out.println(name);
        }
    }


}
