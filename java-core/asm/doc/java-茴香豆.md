
问题:
```
   public static void main(String[] args) throws InterruptedException {
        int i = 0;
        i = i++;
        System.out.println(i);
    }
```
输出内容为`0`

这是一个在JAVA语言层面无法解决的问题。 必须了解字节码的执行过程才能理解输出结果。

本文属于 茴香豆的`茴`有几种写法的研究。  只是为了说明有些问题JAVA语言层面无法解决。可以深入到字节码理解

# 字节码分析
```
public static void main(java.lang.String[]) throws java.lang.InterruptedException;
    descriptor: ([Ljava/lang/String;)V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=2, locals=2, args_size=1
         0: iconst_0  // 常量0 压入操作数栈(0)
         1: istore_1  // 操作数栈(0) 的值 存到局部变量表(1)
         2: iload_1   // 局部变量表(1)的值压入 操作数栈(0) 
         3: iinc          1, 1 // 将局部变量表(1) 的值 加 1
         6: istore_1   // 操作数栈(0) 的值 存到局部变量表(1)
         7: getstatic     #2    // java/lang/System.out 对象压入栈顶
        10: iload_1             // 局部变量表(1)的值压入 压入栈顶
        11: invokevirtual #3    // 执行 System.out.println
        14: return

```

<table>
<tr>
<td>局部变量表(0)</td>
<td>局部变量表(1)</td>
<td>操作数栈(0)</td>
<td>操作数栈(1)</td>
<td>说明</td>
</tr>
<tr>
<td>args</td>
<td></td>
<td>0</td>
<td></td>
<td></td>
</tr>
<tr>
<td>args</td>
<td>0</td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>args</td>
<td>0</td>
<td>0</td>
<td></td>
<td></td>
</tr>
<tr>
<td>args</td>
<td>1</td>
<td>0</td>
<td></td>
<td>(iinc 指令 将局部变量表的值+1)</td>
</tr>
<tr>
<td>args</td>
<td>0</td>
<td>0</td>
<td></td>
<td>istore_1 将 操作数栈顶(0)的值覆盖局部变量表(1)</td>
</tr>
<tr>
<td>args</td>
<td>0</td>
<td>System.out</td>
<td></td>
<td></td>
</tr>
<tr>
<td>args</td>
<td>0</td>
<td>System.out</td>
<td>0</td>
<td></td>
</tr>
<tr>
<td  colspan="5">执行System.out.println(0)</td>
</tr>
</table>