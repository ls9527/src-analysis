# 字节码解析

本节通过一个简单的例子来描述学习字节码的重要性

在学java的时候,学过c++表达式的含义,表达的就是`c=c+1`

那么下面例子的代码输出内容是多少呢?

```
public class LinePlusTest {
    public static void main(String[] args) {
        int c = 5;
        int r = (c++)+(c++)+(++c);
        System.out.println(r);
    }
}
```

按照c++表达式去分析 `5 + 6 + 8 = 19`, 结果应该是19  

如何知道虚拟机的具体执行过程呢? 我们只好解读字节码。

执行反编译命令 `javap -verbose LinePlusTest.java`

获得main函数字节码
```
public static void main(java.lang.String[]);
    descriptor: ([Ljava/lang/String;)V
    flags: ACC_PUBLIC, ACC_STATIC
    Code:
      stack=2, locals=3, args_size=1
         //常量5放入操作数栈
         0: iconst_5
         //弹出栈的值,保存到第2个本地槽
         1: istore_1
         //第二个本地槽的值入栈, 栈顶为5
         2: iload_1
         //第二个槽的值自增1,  结果为6
         3: iinc          1, 1
         //第二个槽的值入栈, 栈顶为6
         6: iload_1
         //第二个槽的值自增1,  结果为7
         7: iinc          1, 1
         // 栈顶的5和6相加，结果入栈，栈顶为11
        10: iadd
        //第二个本地槽自增1，结果为8
        11: iinc          1, 1
        //第二个本地槽的值 8 入栈
        14: iload_1
        // 栈顶的 8 和 11 相加， 将结果19入栈
        15: iadd
        // 将结果存入第三个槽
        16: istore_2
        // 打印结果
        17: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
        20: iload_2
        21: invokevirtual #3                  // Method java/io/PrintStream.println:(I)V
        24: return
      LineNumberTable:
        line 5: 0
        line 6: 2
        line 7: 17
        line 8: 24
      LocalVariableTable:
        Start  Length  Slot  Name   Signature
            0      25     0  args   [Ljava/lang/String;
            2      23     1     c   I
           17       8     2     r   I
}

```