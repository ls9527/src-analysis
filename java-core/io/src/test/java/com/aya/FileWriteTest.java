package com.aya;

import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileWriteTest {

    @Test
    public void testAbsolutePath() {
        System.out.println(System.getProperty("user.dir"));
        File file = new File("readonly.txt");
        System.out.println(file.getAbsolutePath());
    }

    @Test
    public void writeReadOnly() throws IOException {
        System.out.println(System.getProperty("user.dir"));
        File file = new File("d:/readonly.txt");
        if (!file.exists()) {
            file.createNewFile();
        }

        System.out.println(file.canRead());
        System.out.println(file.canWrite());

        try (FileOutputStream fileOutputStream = new FileOutputStream(file);) {
            for (int i = 0; i < 10; i++) {
                fileOutputStream.write("你好".getBytes());
                System.out.println(i);

                fileOutputStream.flush();
                Thread.sleep(2000);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
