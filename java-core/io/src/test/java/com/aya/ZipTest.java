package com.aya;

import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipTest {
    @Test
    public void testZip() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte [] content = "hello".getBytes(Charset.forName("GBK"));
        String name = "QHKY_20181228_244_DD_1.0.0.txt";
        try(ZipOutputStream zipOutputStream = new ZipOutputStream(out);) {
            zipOutputStream.putNextEntry(new ZipEntry(name)); // 设置ZipEntry对象
            zipOutputStream.write(content);
        }
        FileOutputStream fileOutputStream = new FileOutputStream(new File("d:/QHKY_20190102_244_1.zip"));
        byte[] bytes1 = out.toByteArray();
        byte[] bytes = new String(bytes1,0,bytes1.length).getBytes();
        fileOutputStream.write(bytes);
        fileOutputStream.close();
    }
}
