package com.aya;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import sun.nio.ByteBuffered;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomAccessFileTest {


    @Test
    public void testReadLine() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("d:/123.txt","r");
        String content = randomAccessFile.readLine();
        System.out.println(content);
    }

    public static byte[] getBytes(char[] chars) {
      byte [] result = new byte[chars.length];
      for(int i=0;i<chars.length;i++){
          result[i] = (byte) chars[i];
      }
      return result;
    }

    @Test
    public void testDepthReadLine() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("d:/123.txt","r");
        String content = randomAccessFile.readLine();
        byte[] bytes = getBytes(content.toCharArray());
        System.out.println(new String(bytes));
    }

    @Test
    public void testDepthReadLineBytes() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("d:/123.txt","r");
        String content = randomAccessFile.readLine();
        System.out.println(new String(content.getBytes("UTF-8")));
    }

    @Test
    public void testReadLineRepair() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("d:/123.txt","r");
        String content = randomAccessFile.readLine();
        content = new String(content.getBytes(),"GB2312");
        System.out.println(content);
    }

    @Test
    public void testReadBytes() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("d:/123.txt","r");
        byte [] buffer = new byte[1024];
        randomAccessFile.read(buffer);
        System.out.println(new String(buffer));
    }

    @Test
    public void testReadBytesCut() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("d:/123.txt","r");
        byte [] buffer = new byte[3];
        int realLength = 0;
        StringBuilder builder = new StringBuilder();
        while((realLength=randomAccessFile.read(buffer))>0){
            builder.append(new String(buffer,0,realLength));
        }

        System.out.println(builder);
    }

    @Test
    public void testReadBytesBigBytes() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("d:/123.txt","r");

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte [] buffer = new byte[3];
        int realLength = 0;
        while((realLength=randomAccessFile.read(buffer))>0){
            byteArrayOutputStream.write(buffer,0,realLength);
        }

        System.out.println(new String(byteArrayOutputStream.toByteArray()));
    }


    @Test
    public void testCommonsIoUtils() throws IOException {
        final String content = FileUtils.readFileToString(new File("d:/big.txt"), "UTF-8");
        System.out.println(content);
    }

    @Test
    public void testCommonsIoUtilsRandomAccess() throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile("d:/big2.txt", "r");
        final String content = DataInputStream.readUTF(randomAccessFile);
        System.out.println(content);
    }
}
