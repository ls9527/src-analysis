package com.aya;

import org.junit.Test;

public class RefTest {

    @Test
    public void testEquals()  {
        Boolean b = new Boolean(false);
        change(b);
        System.out.println(b);
    }

    private void change(Boolean b)  {
        b = !b;
    }

    @Test
    public void testClass()  {
        Student student = new Student();
        student.name = "name";
        changeClass(student);
        System.out.println(student.name);
    }

    class Student{
        String name;
    }

    private void changeClass(Student stu)  {
        stu = new Student();
        stu.name = "changeClass";
    }



}
