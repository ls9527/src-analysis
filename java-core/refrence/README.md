碰到了一个群内求助关于引用传值的问题.

这里把容易混淆的赋值和类传值问题进行详细说明

# 引用修改
```
    @Test
    public void testEquals()  {
        Boolean b = new Boolean(false);
        change(b);
        System.out.println(b);
    }

    private void change(Boolean b)  {
        b = !b;
    }
```

关于这个例子的答案,熟练java的你一定知道结果是`false`.

那么这是为什么呢,内存的布局是怎样的呢?


![新建](http://cdn.blog.shangwantong.com/java-core/ref/new1.png)
错误理解
![修改堆](http://cdn.blog.shangwantong.com/java-core/ref/changeDui.png)
实际上
![新申请空间](http://cdn.blog.shangwantong.com/java-core/ref/new3.png)
![赋值栈地址](http://cdn.blog.shangwantong.com/java-core/ref/new4.png)

## 疑问解答
为什么不是修改栈的指向?
![赋值栈地址](http://cdn.blog.shangwantong.com/java-core/ref/question1.jpg)
因为不是同一个栈地址,按照 黑色->绿色->红色 的顺序去看
![赋值栈地址](http://cdn.blog.shangwantong.com/java-core/ref/methodStack.png)

# 引用属性修改

```
    @Test
     public void testClass()  {
         Student student = new Student();
         student.name = "name";
         changeClassAttr(student);
         System.out.println(student.name);
     }
 
     class Student{
         String name;
     }
     private void changeClassAttr(Student stu)  {
         stu.name = "changeAttr";
     }

```

![修改属性](http://cdn.blog.shangwantong.com/java-core/ref/classNewStu.png)
![类新申请空间](http://cdn.blog.shangwantong.com/java-core/ref/changeAttr.png)

# 混淆点
根据`引用修改`的理论,那么现在是不是就已经知道这里的答案了呢?
```
    @Test
    public void testClass()  {
        Student student = new Student();
        student.name = "name";
        changeClass(student);
        System.out.println(student.name);
    }

    class Student{
        String name;
    }

    private void changeClass(Student stu)  {
        stu = new Student();
        stu.name = "changeClass";
    }
```

