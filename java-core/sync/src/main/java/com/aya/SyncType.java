package com.aya;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SyncType {
    private static final Logger logger = LoggerFactory.getLogger(SyncType.class);

    public synchronized static void syncStaticA() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("syncStaticA");
    }

    public synchronized static void syncStaticB() {
        logger.info("syncStaticB");
    }

    public synchronized static void syncStaticClass() {
        synchronized (SyncType.class) {
            logger.info("syncStaticClass");
        }
    }

    public synchronized void syncCurrentA() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("syncCurrentA");
    }

    public synchronized void syncCurrentB() {
        logger.info("syncCurrentB");
    }

    public void syncCurrentC() {
        synchronized (this) {
            logger.info("syncCurrentC");
        }
    }
}
