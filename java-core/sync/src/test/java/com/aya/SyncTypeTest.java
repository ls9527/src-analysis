package com.aya;

import org.junit.Test;

public class SyncTypeTest {

    @Test
    public void testStaticSyncA() throws InterruptedException {
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                    SyncType.syncStaticA();
            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                SyncType.syncStaticB();
            }
        });

        threadA.start();
        Thread.sleep(200);
        threadB.start();

        Thread.sleep(3000);
    }

    @Test
    public void testStaticSyncB() throws InterruptedException {
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                    SyncType.syncStaticA();
            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                SyncType.syncStaticClass();
            }
        });

        threadA.start();
        Thread.sleep(200);
        threadB.start();

        Thread.sleep(3000);
    }

    @Test
    public void testCurrentSyncA() throws InterruptedException {
        final SyncType syncType = new SyncType();
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                syncType.syncCurrentA();
            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                syncType.syncCurrentB();
            }
        });

        threadA.start();
        Thread.sleep(200);
        threadB.start();

        Thread.sleep(3000);
    }

    @Test
    public void testCurrentSyncB() throws InterruptedException {
        final SyncType syncType = new SyncType();
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                syncType.syncCurrentA();
            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                syncType.syncCurrentC();
            }
        });

        threadA.start();
        Thread.sleep(500);
        threadB.start();

        Thread.sleep(3000);
    }


    @Test
    public void testCurrentAndStaticSync() throws InterruptedException {
        final   SyncType syncType = new SyncType();
        Thread threadA = new Thread(new Runnable() {
            @Override
            public void run() {
                syncType.syncCurrentA();
            }
        });

        Thread threadB = new Thread(new Runnable() {
            @Override
            public void run() {
                SyncType.syncStaticA();

            }
        });

        threadA.start();
        Thread.sleep(500);
        threadB.start();

        Thread.sleep(3000);
    }
}
