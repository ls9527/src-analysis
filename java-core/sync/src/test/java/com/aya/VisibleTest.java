package com.aya;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * -server 模式下可以体现  voletile 的重要性
 */
public class VisibleTest {

    public static Boolean innerFlag = Boolean.TRUE;

    /**
     * 主线程变更, 内部线程卡死
     * server 模式卡死
     *
     * @throws InterruptedException
     */
    public void testInner() throws InterruptedException {
        final VisibleTest vo = new VisibleTest();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                long count = 0;
                boolean sx = false;
                while (VisibleTest.innerFlag) {
                }
                System.out.println("子线程可见性:" + VisibleTest.innerFlag);
            }
        });

        thread.start();

        TimeUnit.SECONDS.sleep(1);


        System.out.println("主线程可见性 A:" + innerFlag);
        VisibleTest.innerFlag = Boolean.FALSE;
        // 是否对主线程可见
        System.out.println("主线程可见性 B:" + innerFlag);


        TimeUnit.SECONDS.sleep(3);

    }


    public static  Boolean outerFlag = Boolean.TRUE;
    /**
     * 内部线程变更, 主线程卡死
     * server 模式卡死
     *
     * @throws InterruptedException
     */
    @Test
    public void testOuter() throws InterruptedException {
        final VisibleTest vo = new VisibleTest();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("子线程可见性 start:" + VisibleTest.outerFlag);
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                }
                VisibleTest.outerFlag = Boolean.FALSE;
                System.out.println("子线程可见性 end:" + VisibleTest.outerFlag);
            }
        });

        thread.start();


        System.out.println("主线程可见性 A:" + outerFlag);

        while (VisibleTest.outerFlag) {
        }

        // 是否对主线程可见
        System.out.println("主线程可见性 B:" + outerFlag);


        TimeUnit.SECONDS.sleep(3);

    }


}
