package com.aya;

import org.junit.Assert;
import org.junit.Test;

public class SystemOutTest {
    @Test
    public void testA() throws InterruptedException {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while(true){
                    System.out.println("你好世界".getBytes());
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        new Thread(runnable).start();

        Thread.sleep(200000);
    }

}
