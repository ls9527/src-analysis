package com.aya;

public class CacheApp {
    public static void main(String[] args) {
        Boolean.valueOf(false);

        Character.valueOf('a');

        Byte.valueOf((byte)10);
        Short.valueOf((short)20);
        Integer ia = Integer.valueOf(12);
        Long.valueOf(10);


        Float.valueOf(10.0f);
        Double.valueOf(10);
    }
}
