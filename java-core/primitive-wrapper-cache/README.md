# 8大基本类型的包装类型缓存探究

缓存这里分为三种
1. 自身范围缓存
    - boolean: true,false
    - char: ascii码
2. 范围为256的缓存,其中 int的缓存可以通过参数配置上限
    - byte
    - short
    - int
    - long
3. 不被缓存
    - float
    - double
 
# 自身范围缓存
## boolean
boolean 的包装类型,缓存最简单,直接定义为静态常量就可以
```java
public final class Boolean implements java.io.Serializable,
                                      Comparable<Boolean>
{
    public static final Boolean TRUE = new Boolean(true);

    public static final Boolean FALSE = new Boolean(false);
    
    public static Boolean valueOf(boolean b) {
        return (b ? TRUE : FALSE);
    }
}
```
## char
ascii 码范围为 0-127, 这里只缓存ascii码范围的char
Character 静态内部类 CharacterCache

不过通过下面这段代码可以看出来，他的范围是: 0<=x<=127
```java
public final
class Character implements java.io.Serializable, Comparable<Character> {
    
    public static Character valueOf(char c) {
        if (c <= 127) { // must cache
            return CharacterCache.cache[(int)c];
        }
        return new Character(c);
    }
    
    private static class CharacterCache {
        private CharacterCache(){}

        static final Character cache[] = new Character[127 + 1];

        static {
            for (int i = 0; i < cache.length; i++)
                cache[i] = new Character((char)i);
        }
    }
}

```

## byte
byte 的范围是: -128<=x<=127

所以byte 一定是从缓存里面获取

-(-128) + 127 + 1 这一段的意思就是说 (-128~-1)+(1~127)+0 一共256个数  
```java
public final class Byte extends Number implements Comparable<Byte> {
  public static Byte valueOf(byte b) {
        final int offset = 128;
        return ByteCache.cache[(int)b + offset];
  }
    
  private static class ByteCache {
      private ByteCache(){}

      static final Byte cache[] = new Byte[-(-128) + 127 + 1];

      static {
          for(int i = 0; i < cache.length; i++)
              cache[i] = new Byte((byte)(i - 128));
      }
  }
}
```

# 范围为256的缓存
## short
和byte 范围一样, 但是如果超过了 -128<=x<=127 这个范围,就不会从缓存中获取了
```java
public final class Short extends Number implements Comparable<Short> {
    public static Short valueOf(short s) {
        final int offset = 128;
        int sAsInt = s;
        if (sAsInt >= -128 && sAsInt <= 127) { // must cache
            return ShortCache.cache[sAsInt + offset];
        }
        return new Short(s);
    }
    
    private static class ShortCache {
        private ShortCache(){}

        static final Short cache[] = new Short[-(-128) + 127 + 1];

        static {
            for(int i = 0; i < cache.length; i++)
                cache[i] = new Short((short)(i - 128));
        }
    }
}
```
## int
这里的从缓存中获取的代码是一样的,重点是 IntegerCache 的范围是可以配置的

从源码中可以看出来 最低值一定是-128 是不可以修改的,但是上限值high 是可以修改的。

通过jvm参数: -Djava.lang.Integer.IntegerCache.high=1024 修改为1024

1. 判断是否有jvm参数java.lang.Integer.IntegerCache.high
2. 解析为 int 类型(解析失败就用默认值127)
3. 修改最大值和最小值
    - 如果小于127 那么 high = 127
    - 如果 大于 Integer.MAX_VALUE - 129 ,就设置为  Integer.MAX_VALUE - 129
4. 将 low<=x<=high的值加入到缓存
5. 验证  IntegerCache.high > 127
```java
public final class Integer extends Number implements Comparable<Integer> {
    public static Integer valueOf(int i) {
        if (i >= IntegerCache.low && i <= IntegerCache.high)
            return IntegerCache.cache[i + (-IntegerCache.low)];
        return new Integer(i);
    }
    
     private static class IntegerCache {
            static final int low = -128;
            static final int high;
            static final Integer cache[];
    
            static {
                // high value may be configured by property
                int h = 127;
                String integerCacheHighPropValue =
                    sun.misc.VM.getSavedProperty("java.lang.Integer.IntegerCache.high");
                if (integerCacheHighPropValue != null) {
                    try {
                        int i = parseInt(integerCacheHighPropValue);
                        i = Math.max(i, 127);
                        // Maximum array size is Integer.MAX_VALUE
                        h = Math.min(i, Integer.MAX_VALUE - (-low) -1);
                    } catch( NumberFormatException nfe) {
                        // If the property cannot be parsed into an int, ignore it.
                    }
                }
                high = h;
    
                cache = new Integer[(high - low) + 1];
                int j = low;
                for(int k = 0; k < cache.length; k++)
                    cache[k] = new Integer(j++);
    
                // range [-128, 127] must be interned (JLS7 5.1.7)
                assert IntegerCache.high >= 127;
            }
    
            private IntegerCache() {}
      }
}
```
这里的 IntegerCache 无法断点,因为在进入main方法之前,就已经被其他的类加载了.
## long
范围: -128<=x<=127
```java
public final class Long extends Number implements Comparable<Long> {
    public static Long valueOf(long l) {
        final int offset = 128;
        if (l >= -128 && l <= 127) { // will cache
            return LongCache.cache[(int)l + offset];
        }
        return new Long(l);
    }
    
    private static class LongCache {
            private LongCache(){}
    
            static final Long cache[] = new Long[-(-128) + 127 + 1];
    
            static {
                for(int i = 0; i < cache.length; i++)
                    cache[i] = new Long(i - 128);
            }
     }
}
```
# 不被缓存
valueOf 时, 直接new 包装类型的对象,然后返回

## float
```java
public final class Float extends Number implements Comparable<Float> {
  public static Float valueOf(float f) {
        return new Float(f); 
  }
}
```
## double
```java
public final class Double extends Number implements Comparable<Double> {
    public static Double valueOf(double d) {
        return new Double(d);
    }
}
```