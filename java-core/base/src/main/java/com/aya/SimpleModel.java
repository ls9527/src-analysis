package com.aya;

import java.io.Serializable;

public class SimpleModel  implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
