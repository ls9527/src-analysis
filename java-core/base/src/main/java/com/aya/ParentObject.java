package com.aya;

import java.io.Serializable;

public class ParentObject implements Serializable {
    String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
