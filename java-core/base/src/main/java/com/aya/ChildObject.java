package com.aya;

import java.io.Serializable;

public class ChildObject extends ParentObject implements Serializable {
    String text;

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
