package com.aya;

import java.io.Serializable;

/**
 * @author zww
 */
public class SerialModel implements Serializable {
    public SerialModel(){}
    private static final long serialVersionUID = 0x4;

    private String name;

    private String id;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getZz() {
        return zz;
    }

    public void setZz(String zz) {
        this.zz = zz;
    }

    private String zz;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SerialModel{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", zz='" + zz + '\'' +
                '}';
    }
}
