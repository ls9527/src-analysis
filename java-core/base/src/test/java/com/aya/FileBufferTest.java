package com.aya;

import org.junit.Test;

import java.io.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class FileBufferTest {

    @Test
    public void testFileRead() throws IOException {

        BufferedReader bufferedInputStream = new BufferedReader(new InputStreamReader(
                new FileInputStream("e:/store/buffer.txt"),"UTF-8"),30);
        String s = bufferedInputStream.readLine();
        System.out.println(s);
    }


}
