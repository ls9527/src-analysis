package com.aya;

import org.junit.Test;

import java.io.*;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.*;

public class FutureTest {

    @Test
    public void testSerialString() throws IOException, ExecutionException, InterruptedException, TimeoutException {
        FutureTask<String> f1 = new FutureTask<String>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(3000);
                return "123";
            }
        });

        ExecutorService executorService = Executors.newFixedThreadPool(5);
        executorService.submit(f1);
        String s = f1.get(3L, TimeUnit.SECONDS);

        System.out.println(s);
    }

    @Test
    public void testSerialStringB() throws IOException, ExecutionException, InterruptedException, TimeoutException {
        ExecutorService executorService1 = Executors.newFixedThreadPool(3);
        CompletionService<String> completionService = new ExecutorCompletionService<>(executorService1);
        Callable<String> callable5 = new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(5000);
                return "5";
            }
        };

        Callable<String> callable3 = new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(3000);
                return "3";
            }
        };

        Callable<String> callable1 = new Callable<String>() {
            @Override
            public String call() throws Exception {
                Thread.sleep(1000);
                return "1";
            }
        };

        Date start = new Date();
        completionService.submit(callable5);
        completionService.submit(callable3);
        completionService.submit(callable1);

        Future<String> take = completionService.take();
        Future<String> take1 = completionService.take();
        Future<String> take2 = completionService.take();

//        String s = f1.get(3L, TimeUnit.SECONDS);
        Date end = new Date();
        System.out.println(take.get());
        System.out.println(end.getTime()-start.getTime());
    }


    @Test
    public void testCircle() throws BrokenBarrierException, InterruptedException {
        final CyclicBarrier barrier=new CyclicBarrier(5);
        //启用5个线程
        for(int i=1;i<=9;i++){
            new Thread(new Runnable(){
                public void run(){
                    try {
                        Random random = new Random();
                        int i1 = random.nextInt(5)+1;
                        System.out.println("second:"+i1);
                        Thread.sleep(1000* i1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println("子线程执行！");
                    try {
                        barrier.await();//到达屏障
                    } catch (InterruptedException | BrokenBarrierException e) {
                        e.printStackTrace();
                    }

                }
            }).start();

        }
        //主线程
        Date start = new Date();
        barrier.await();//阻塞当前线程直到latch中数值为零才执行
        Date end = new Date();
        System.out.println(end.getTime()-start.getTime());
        System.out.println("主线程执行！");
    }




    @Test
    public void testCountDown() throws BrokenBarrierException, InterruptedException {
        final CountDownLatch barrier=new CountDownLatch(5);
        //启用5个线程
        for(int i=1;i<=5;i++){
            new Thread(new Runnable(){
                public void run(){
                    try {
                        Random random = new Random();
                        int i1 = random.nextInt(5)+1;
                        System.out.println("second:"+i1);
                        Thread.sleep(1000* i1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println("子线程执行！");
                    barrier.countDown();//到达屏障
                }
            }).start();

        }
        //主线程
        Date start = new Date();
        barrier.await();//阻塞当前线程直到latch中数值为零才执行
        Date end = new Date();
        System.out.println(end.getTime()-start.getTime());
        System.out.println("主线程执行！");
    }
}
