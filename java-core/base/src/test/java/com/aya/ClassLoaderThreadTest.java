package com.aya;

import org.junit.Test;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;

public class ClassLoaderThreadTest {


    @Test
    public void testUrlLoader() throws MalformedURLException, InterruptedException {
        Thread t=  new Thread(new Runnable() {
            @Override
            public void run() {
                ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                URL resource = contextClassLoader.getResource("123.txt");
                System.out.println("123.txt:"+resource);

                try {
                    Enumeration<URL> resources = contextClassLoader.getResources("");
                    while (resources.hasMoreElements()){
                        URL emptyResource = resources.nextElement();
                        System.out.println("empty:"+ emptyResource);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        URLClassLoader cl = new URLClassLoader(new URL[]{new URL("file://c:/tools/")});
        t.setContextClassLoader(cl);
        t.start();
        t.join();
    }
}
