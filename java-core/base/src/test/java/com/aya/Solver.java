package com.aya;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class Solver {
    final int N;    //矩阵的行数
    final float[][] data;    //要处理的矩阵
    final CyclicBarrier barrier;    //循环屏障

    class Worker implements Runnable {
        int myRow;
        public Worker(int row) { myRow = row; }
        public void run() {
            System.out.println("处理数据"+myRow);

            try {
                barrier.await();     //在屏障处等待直到
            } catch (InterruptedException ex) {
                return;
            } catch (BrokenBarrierException ex) {
                return;
            }
        }
    }

    public Solver(float[][] matrix) throws BrokenBarrierException, InterruptedException {
        data = matrix;
        N = matrix.length;
        //初始化CyclicBarrier
        barrier = new CyclicBarrier(N, new Runnable() {
            public void run() {
//                mergeRows(...); //合并行
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("做完了所有事情");
            }
        });
        for (int i = 0; i < N; ++i)
            new Thread(new Worker(i)).start();

//        waitUntilDone();
        System.out.println("等待到完成");
    }

    public static void main(String[] args) throws BrokenBarrierException, InterruptedException {
       new Solver(new float[][]{{1,2,3},{2,2,2}});
    }
}
