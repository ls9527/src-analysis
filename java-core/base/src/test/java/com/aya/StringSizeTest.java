package com.aya;

import org.junit.Test;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class StringSizeTest {
   @Test
    public void testSize(){
       byte bytes [] = new byte [1024*1024*10];
       String str = new String(bytes);
       doA(str);
   }

    private void doA(String str) {
       doB(str);
    }

    private void doB(String str) {
        doC(str);
    }

    private void doC(String str) {
        doD(str);
    }

    private void doD(String str) {
        try {
            Thread.sleep(1000*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
