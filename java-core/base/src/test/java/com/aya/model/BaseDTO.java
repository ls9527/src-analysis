package com.aya.model;

/**
 * @author liushang@zsyjr.com
 * @version 1.0
 * @description 描述信息
 * @date 2019/10/14
 **/

import java.io.Serializable;

public abstract class BaseDTO implements Serializable {

    protected static final long serialVersionUID = 1L;

}