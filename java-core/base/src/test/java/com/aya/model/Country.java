package com.aya.model;

/**
 * @author liushang@zsyjr.com
 * @version 1.0
 * @description 描述信息
 * @date 2019/11/1
 **/
public enum Country {
    CHINA, ENGLISH, KOREAN, JAP
}
