package com.aya;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Function;

public class DecimalTest {

    @Test
    public void testDecimalSimple() {
        BigDecimal a = new BigDecimal(5);
        BigDecimal b = new BigDecimal(40);
        BigDecimal add = a.add(b);
        BigDecimal subtract = a.subtract(b);
        BigDecimal multiply = a.multiply(b);
        BigDecimal divide = a.divide(b);
        System.out.println("add:" + add);
        System.out.println("subtract:" + subtract);
        System.out.println("multiply:" + multiply);
        System.out.println("divide:" + divide);
    }

    @Test
    public void testDoubleSimple() {
        double a = 3;
        double b = 10;
        double c = a / b;
        System.out.println(c);
    }

    @Test
    public void testOne() {
        BigDecimal divide = BigDecimal.valueOf(1).divide(BigDecimal.valueOf(100), 2, RoundingMode.DOWN);
        System.out.println(divide);
    }


    @Test
    public void testRound() {
        // 正无穷大方向取整
        System.out.println("celling:" + new BigDecimal(0.125, new MathContext(2, RoundingMode.CEILING)));
        // 负无穷大方向取整
        System.out.println("floor:" + new BigDecimal(0.125, new MathContext(2, RoundingMode.FLOOR)));
        //向 0 的方向取整
        System.out.println("down a:" + new BigDecimal(0.121, new MathContext(2, RoundingMode.DOWN)));
        System.out.println("down b:" + new BigDecimal(-0.129, new MathContext(2, RoundingMode.DOWN)));
        // 正数向正无穷大取整，负数向负无穷大取整
        System.out.println("up a:" + new BigDecimal(0.121, new MathContext(2, RoundingMode.UP)));
        System.out.println("up b:" + new BigDecimal(-0.129, new MathContext(2, RoundingMode.UP)));
        /**
         * 5,6,7,8,9 向上取整
         * 1,2,3,4 向下取整
         *
         * 常用的4舍5入
         */
        System.out.println("half up:" + new BigDecimal(0.125, new MathContext(2, RoundingMode.HALF_UP)));
        /**
         *  6,7,8,9 向上取整
         *  1,2,3,4,5 向下取整
         */
        System.out.println("half down:" + new BigDecimal(0.125, new MathContext(2, RoundingMode.HALF_DOWN)));

        /**
         * 小数位是5时，判断整数部分是奇数就进位
         * 1,2,3,4,  舍弃
         * 6,7,8,9,  进位
         */
        System.out.println("odd a:" + new BigDecimal(5.4, new MathContext(1, RoundingMode.HALF_EVEN)));
        System.out.println("odd b:" + new BigDecimal(5.5, new MathContext(1, RoundingMode.HALF_EVEN)));
        /**
         * 小数位是5时，判断整数部分是偶数就舍弃
         * 1,2,3,4,  舍弃
         * 6,7,8,9,  进位
         */
        System.out.println("even a:" + new BigDecimal(6.5, new MathContext(1, RoundingMode.HALF_EVEN)));
        System.out.println("even b:" + new BigDecimal(6.6, new MathContext(1, RoundingMode.HALF_EVEN)));
    }


    /**
     * 小数除法
     */
    @Test
    public void testDecimalDivide() {
        BigDecimal a = new BigDecimal(5.4);
        BigDecimal b = new BigDecimal(3.1);
        BigDecimal divide = a.divide(b);
        System.out.println("divide:" + divide);
    }


    @Test
    public void testDecimalStandDivide() {
        BigDecimal a = new BigDecimal(5.4);
        BigDecimal b = new BigDecimal(3.1);
        // 保留几位小数
        int scale = 2;
        BigDecimal divide = a.divide(b,scale,RoundingMode.HALF_UP);
        System.out.println("divide:" + divide);

    }


    @Test
    public void testOneShare() {
        System.out.println(oneShare(1));
        System.out.println(oneShare(2));
        System.out.println(oneShare(3));
        System.out.println(oneShare(4));
    }


    private BigDecimal oneShare(Integer volPrecision){
        BigDecimal oneShare = BigDecimal.ONE ;
        if(volPrecision == null  || volPrecision == 0){
            return oneShare ;
        }else {
            return  oneShare.divide(BigDecimal.TEN.pow(volPrecision),volPrecision,BigDecimal.ROUND_DOWN);
        }
    }


}
