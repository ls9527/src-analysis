package com.aya;

import com.aya.model.Country;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author liushang@zsyjr.com
 * @version 1.0
 * @description 测试枚举反射
 * @date 2019/11/1
 **/
public class EnumTest {

    @Test
    public void testEnumClassSuccess() {
        Class<Country> countryClass = Country.class;

        Country china = Enum.valueOf(countryClass, "CHINA");

        Assert.assertTrue("枚举值不一致", china.equals(Country.CHINA));
    }

    @Test
    public void testEnumClassNotExists() {
        Class<Country> countryClass = Country.class;

        Country china = Enum.valueOf(countryClass, "CHINA_HAHAHA");

        Assert.assertTrue("枚举值不一致", china.equals(Country.CHINA));
    }

    /**
     * 利用泛型擦除的形式转换
     */
    @Test
    public void testEnumClassAmbiguous() {
        Class<? extends Enum<?>> countryClass = Country.class;

        Enum china = Enum.valueOf((Class)countryClass, "CHINAXX");

        Assert.assertTrue("枚举值不一致", china.equals(Country.CHINA));
    }
}
