package com.aya;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import java.io.UnsupportedEncodingException;

/**
 * @author liushang@zsyjr.com
 * @version 1.0
 * @description 描述信息
 * @date 2019/11/20
 **/
public class RsaTest {

    @Test
    public void testRsa() throws UnsupportedEncodingException {
        String pubKey = "67631c8d9923dd529875c5b5d1f802f05fb4807bb644d1def9b1d92c6139c527e342e79670083c8055d2e4cfe8f485950eb89375a9a09c6bcf7210377d0a01aae29a53f9bbe9c86a49e0b6693420d6688d64de5f1bd1917fc418afd06c4e5e1aa7d80031690eac78833dfe5c0b8f4d4f015ed971d8cade611b460c347c13ca3e";
        System.out.println(hexStr2Str(pubKey));
    }

    public static String hexStr2Str(String hexStr) throws UnsupportedEncodingException {
        String str = "0123456789ABCDEF";
        char[] hexs = hexStr.toCharArray();
        byte[] bytes = new byte[hexStr.length() / 2];
        int n;
        for (int i = 0; i < bytes.length; i++) {
            n = str.indexOf(hexs[2 * i]) * 16;
            n += str.indexOf(hexs[2 * i + 1]);
            bytes[i] = (byte) (n & 0xff);
        }
        return new String(bytes, "UTF-8");
    }

    @Test
    public void testCpy() {
        String string = "qianyang123";
        //编码
        String encode = encode(string.getBytes());
        System.out.println(string + "\t编码后的字符串为：" + encode);
        //解码
        String decode = decode(encode.getBytes());
        System.out.println(encode + "\t字符串解码后为：" + decode);
    }

    //base64 解码
    public static String decode(byte[] bytes) {
        return new String(Base64.decodeBase64(bytes));
    }

    //base64 编码
    public static String encode(byte[] bytes) {
        return new String(Base64.encodeBase64(bytes));
    }
}
