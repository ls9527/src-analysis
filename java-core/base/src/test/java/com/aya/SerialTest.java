package com.aya;

import com.aya.haha.SerialEmptyModel;
import com.aya.model.FieldChangeModel;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SerialTest {

    @Test
    public void testSerialString() throws IOException {

        String name = "Karen Aya";
        try (ByteArrayOutputStream byteArrayInputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectInputStream = new ObjectOutputStream(byteArrayInputStream)) {

            objectInputStream.writeObject(name);
            byte[] result = byteArrayInputStream.toByteArray();

            FileOutputStream fileOutputStream = new FileOutputStream(new File("d:/stringSerial.txt"));
            fileOutputStream.write(result, 0, result.length);
            fileOutputStream.close();
        }
    }

    /**
     * @throws IOException
     */
    @Test
    public void testSerialChild() throws IOException {
        ChildObject childObject = new ChildObject();
        childObject.setText("text");
        childObject.setName("name");
        try (ByteArrayOutputStream byteArrayInputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectInputStream = new ObjectOutputStream(byteArrayInputStream)) {

            objectInputStream.writeObject(childObject);
            byte[] result = byteArrayInputStream.toByteArray();

            FileOutputStream fileOutputStream = new FileOutputStream(new File("d:/childObject.txt"));
            fileOutputStream.write(result, 0, result.length);
            fileOutputStream.close();
        }
    }

    @Test
    public void testDeSerialChild() throws IOException {

        try (InputStream inputStream = new FileInputStream("d:/childObject.txt");
             ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);) {
            Object object = objectInputStream.readObject();

            System.out.println(object);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSerialDate() throws IOException {

        Date date = new Date();
        try (ByteArrayOutputStream byteArrayInputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectInputStream = new ObjectOutputStream(byteArrayInputStream)) {

            objectInputStream.writeObject(date);
            byte[] result = byteArrayInputStream.toByteArray();

            FileOutputStream fileOutputStream = new FileOutputStream(new File("d:/stringSerial.txt"));
            fileOutputStream.write(result, 0, result.length);
            fileOutputStream.close();
        }
    }

    @Test
    public void testWriteClass() throws IOException {

        byte[] objData;
        try (ByteArrayOutputStream byteArrayInputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectInputStream = new ObjectOutputStream(byteArrayInputStream)) {

            SerialModel serialModel = new SerialModel();
            serialModel.setName("Karen");
            objectInputStream.writeObject(serialModel);
            objData = byteArrayInputStream.toByteArray();
        }

        FileOutputStream fileOutputStream = new FileOutputStream(new File("d:/buf.txt"));
        fileOutputStream.write(objData, 0, objData.length);
        fileOutputStream.close();

        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(objData);
             ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {
            SerialModel serialModel1 = (SerialModel) objectInputStream.readObject();
            System.out.println(serialModel1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }


    @Test
    public void testReadClass() throws IOException {

        FileInputStream fileOutputStream = new FileInputStream(new File("d:/buf.txt"));

        try (ObjectInputStream objectInputStream = new ObjectInputStream(fileOutputStream)) {
            SerialModel serialModel1 = (SerialModel) objectInputStream.readObject();
            System.out.println(serialModel1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        fileOutputStream.close();

    }

    @Test
    public void testLookup() throws IOException {
        ObjectStreamClass lookup = ObjectStreamClass.lookup(SerialModel.class);
        System.out.println(Long.toHexString(lookup.getSerialVersionUID()).toUpperCase());

    }

    class MyClassLoader extends ClassLoader {

    }

    @Test
    public void testHex() throws ClassNotFoundException {
        ObjectStreamClass lookup = ObjectStreamClass.lookup(SerialEmptyModel.class);
        System.out.println(Long.toHexString(lookup.getSerialVersionUID()).toUpperCase());

        //与类名有关
        ObjectStreamClass lookupB = ObjectStreamClass.lookup(SerialEmptyModel.class);
        System.out.println(Long.toHexString(lookupB.getSerialVersionUID()).toUpperCase());
        // 与类加载器无关
        Class<?> aClass = new MyClassLoader().loadClass("com.aya.SerialEmptyModel");
        ObjectStreamClass lookupC = ObjectStreamClass.lookup(aClass);
        System.out.println(Long.toHexString(lookupC.getSerialVersionUID()).toUpperCase());

    }

    /**
     * 测试继承是否会影响序列化ID
     *
     * @throws ClassNotFoundException
     */
    @Test
    public void testExtendsSerialId() throws ClassNotFoundException {
        // 只有字段name的序列化id: 4739163BE983BD92  无法继承自父类的序列化id
        // 有字段 name 和 color 的序列化id:AF398A06DC364D5D
        ObjectStreamClass lookup = ObjectStreamClass.lookup(FieldChangeModel.class);
        System.out.println(Long.toHexString(lookup.getSerialVersionUID()).toUpperCase());
    }

    @Test
    public void testHexClass() {
        long l = Long.parseLong("ca90121b", 16);
        int xxz = Integer.parseInt("0ca90121b", 16);
        System.out.println(xxz);
    }

    @Test
    public void testSha1() throws NoSuchMethodException, NoSuchFieldException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);


        ObjectStreamClass lookup = ObjectStreamClass.lookup(com.aya.SerialEmptyModel.class);
        System.out.println("正确的seial:" + Long.toHexString(lookup.getSerialVersionUID()).toUpperCase());


        StringBuilder stringBuilder = new StringBuilder();
        Class<com.aya.SerialEmptyModel> serialModelClass = com.aya.SerialEmptyModel.class;
        dataOutputStream.writeUTF(serialModelClass.getName());
        dataOutputStream.writeInt(serialModelClass.getModifiers());
        dataOutputStream.writeUTF(Serializable.class.getName());

//        Field name = serialModelClass.getDeclaredField("name");
////        name.setAccessible(true);
//        stringBuilder.append(name.getName());
//        stringBuilder.append(name.getModifiers());
//        String strDescriptor = "Ljava/lang/String";
//        stringBuilder.append(strDescriptor).append(";");
        //6
        Constructor<?> constructor = serialModelClass.getConstructor();
        dataOutputStream.writeUTF("<init>");
        dataOutputStream.writeInt(constructor.getModifiers());
        dataOutputStream.writeUTF("()V;");

        //7
//        stringBuilder.append(addDeclalingMethodInfo(Object.class.getDeclaredMethods()));
        addDeclalingMethodInfo(serialModelClass.getMethods(), dataOutputStream);

        String classContent = byteArrayOutputStream.toString();
        System.out.println(classContent);
//        String admin = SHA1.encode(classContent);
        byte[] byteArray = SHA1.encodeBytes(byteArrayOutputStream.toByteArray());
        String admin = SHA1.getFormattedText(byteArray);
        int[] sha = new int[5];
        for (int i = 0, j = 0; i < 40; i += 8, j++) {
            String strInt = admin.substring(i, i + 8);
            int integer = (int) Long.parseLong(strInt, 16);
            sha[j] = integer;
        }

//        int [] sha = new int [5];
//        for(int i=0,j=0;i<20;i+=4,j++){
//            sha[j] = bytesToInt2(byteArray,i);
//        }
        long hash =
                ((sha[0] >>> 24) & 0xFF) |
                        ((sha[0] >>> 16) & 0xFF) << 8 |
                        ((sha[0] >>> 8) & 0xFF) << 16 |
                        ((sha[0] >>> 0) & 0xFF) << 24 |
                        ((sha[1] >>> 24) & 0xFF) << 32 |
                        ((sha[1] >>> 16) & 0xFF) << 40 |
                        ((sha[1] >>> 8) & 0xFF) << 48 |
                        ((sha[1] >>> 0) & 0xFF) << 56;

        System.out.println("生成的seial:" + Long.toHexString(hash).toUpperCase());

        dataOutputStream.close();
        byteArrayOutputStream.close();

    }


    @Test
    public void testBytes() {
        int i = 10020;
        byte[] bytes = intToBytes(i);
        int i1 = bytesToInt(bytes, 0);
        System.out.println(i == i1);
    }

    public static byte[] intToBytes(int value) {
        byte[] src = new byte[4];
        src[3] = (byte) ((value >> 24) & 0xFF);
        src[2] = (byte) ((value >> 16) & 0xFF);
        src[1] = (byte) ((value >> 8) & 0xFF);
        src[0] = (byte) (value & 0xFF);
        return src;
    }

    public static int bytesToInt(byte[] src, int offset) {
        int value;
        value = (int) ((src[offset] & 0xFF)
                | ((src[offset + 1] & 0xFF) << 8)
                | ((src[offset + 2] & 0xFF) << 16)
                | ((src[offset + 3] & 0xFF) << 24));
        return value;
    }

    public static int bytesToInt2(byte[] src, int offset) {
        int value;
        value = (int) (((src[offset] & 0xFF) << 24)
                | ((src[offset + 1] & 0xFF) << 16)
                | ((src[offset + 2] & 0xFF) << 8)
                | (src[offset + 3] & 0xFF));
        return value;
    }

    /**
     * 将int数值转换为占四个字节的byte数组，本方法适用于(高位在前，低位在后)的顺序。  和bytesToInt2（）配套使用
     */
    public static byte[] intToBytes2(int value) {
        byte[] src = new byte[4];
        src[0] = (byte) ((value >> 24) & 0xFF);
        src[1] = (byte) ((value >> 16) & 0xFF);
        src[2] = (byte) ((value >> 8) & 0xFF);
        src[3] = (byte) (value & 0xFF);
        return src;
    }

    private void addDeclalingMethodInfo(Method[] objMethods, DataOutputStream dataOutputStream) throws IOException {
        List<Method> collect = Stream.of(objMethods).sorted((m1, m2) -> {
            int i = m1.getName().compareTo(m2.getName());
            if (i == 0) {
                return m1.getParameterCount() - m2.getParameterCount();
            }
            return i;
        }).collect(Collectors.toList());
        for (Method method : collect) {
            dataOutputStream.writeUTF(method.getName());
            dataOutputStream.writeInt(method.getModifiers());
            dataOutputStream.writeUTF(getDescriptor(method));
        }
    }

    private String getDescriptor(Method method) {
        StringBuilder stringBuilder = new StringBuilder();

        Parameter[] parameters = method.getParameters();
        String collect = Stream.of(parameters).map((parameter) -> {
            return getDescriptorByType(parameter.getType());
        }).collect(Collectors.joining(",", "(", ")"));

        stringBuilder.append(collect);
        Class<?> returnType = method.getReturnType();
        stringBuilder.append(getDescriptorByType(returnType));
        stringBuilder.append(";");
        return stringBuilder.toString();
    }

    private String getDescriptorByType(Class<?> returnType) {
        if (returnType == void.class) {
            return "V";
        } else if (returnType == long.class) {
            return "J";
        } else if (returnType == int.class) {
            return "I";
        } else if (returnType == boolean.class) {
            return "Z";
        }
        String s = returnType.getName().replaceAll("\\.", "/");
        return "L" + s;
    }

    public static String str2HexStr(String str) {
        char[] chars = "0123456789ABCDEF".toCharArray();
        StringBuilder sb = new StringBuilder("");
        byte[] bs = str.getBytes();
        int bit;
        for (int i = 0; i < bs.length; i++) {
            bit = (bs[i] & 0x0f0) >> 4;
            sb.append(chars[bit]);
            bit = bs[i] & 0x0f;
            sb.append(chars[bit]);
            // sb.append(' ');
        }
        return sb.toString().trim();
    }

}
