package com.aya;

import org.junit.Test;

import java.security.AccessController;
import java.security.PrivilegedAction;

public class AccessTest
{
    @Test
    public void testAccess(){
        SecurityManager securityManager = new SecurityManager();
        Object o = AccessController.doPrivileged(new PrivilegedAction<Object>() {
            @Override
            public Object run() {
                return new SerialModel();
            }
        });
        System.out.println(o);
    }
}
