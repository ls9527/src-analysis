package com.aya;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * @author: liushang@zsyjr.com
 * @description:
 * @date: Created in 19:30 2018/10/23
 */
public class CountDownTest {
    @Test
    public void testCount() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(0);
        countDownLatch.await();
        System.out.println("end");
    }
}
