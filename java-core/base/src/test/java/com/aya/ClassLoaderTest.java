package com.aya;

import org.junit.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class ClassLoaderTest {

    /**
     * app classloader 不存在
     * -Dloader_path=D:/IdeaProjects/src-analysis/java-core/base/loader_test
     * @throws ClassNotFoundException
     */
    @Test
    public void testUrlLoaderNotEquals() throws ClassNotFoundException, MalformedURLException {
        final String property = System.getProperty("loader_path");
        final URL resource = new File(property).toURL();
        URLClassLoader classLoaderA = new URLClassLoader(new URL[]{resource});
        URLClassLoader classLoaderB = new URLClassLoader(new URL[]{resource});

        final Class<?> classFromClassLoaderA = classLoaderA.loadClass("com.aya.Student");
        final Class<?> classFromClassLoaderB = classLoaderB.loadClass("com.aya.Student");

        System.out.println(classFromClassLoaderA.equals(classFromClassLoaderB));
    }

    /**
     * app classloader 存在
     * -Dloader_path=D:/IdeaProjects/src-analysis/java-core/base/loader_test
     * @throws ClassNotFoundException
     */
    @Test
    public void testUrlLoaderEquals() throws ClassNotFoundException, MalformedURLException {
        final String property = System.getProperty("loader_path");
        final URL resource = new File(property).toURL();
        URLClassLoader classLoaderA = new URLClassLoader(new URL[]{resource});
        URLClassLoader classLoaderB = new URLClassLoader(new URL[]{resource});

        final Class<?> classFromClassLoaderA = classLoaderA.loadClass("com.aya.ParentObject");
        final Class<?> classFromClassLoaderB = classLoaderB.loadClass("com.aya.ParentObject");

        System.out.println(classFromClassLoaderA.equals(classFromClassLoaderB));
    }
}
