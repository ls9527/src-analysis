package com.aya;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * hello
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Star {
    String value() default "hello";

    String name() default "name a";

    String desc() default "desc a";

}
