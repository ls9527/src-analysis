package com.aya.character;

import java.io.UnsupportedEncodingException;

/**
 * @author ls9527
 * @date ${DATE}
 */
public class Main {
    public static void main(String[] args) throws UnsupportedEncodingException {
        // 一般字符
        printLength("a");
        // 中文字符
        printLength("张");
    }

    /**
     * 字符的定义: 单一能显示的内容.
     * 字节: 0-255之间的一个数
     * 字符与字节的关系: 不同的字符集对应的字节长度不一样
     * <p>
     * 英文字符在   ISO-8859-1 字符集占用1个字节 , 能还原
     * 英文字符在   GBK 字符集占用1个字节 , 能还原
     * 英文字符在   UTF-8 字符集占用1个字节 , 能还原
     * 英文字符在   UNICODE 字符集占用4个字节 , 能还原
     * <p>
     * 中文字符在   ISO-8859-1 字符集占用1个字节 , 不能还原
     * 中文字符在   GBK 字符集占用2个字节  , 能还原
     * 中文字符在   UTF-8 字符集占用3个字节 , 能还原
     * 中文字符在   UNICODE 字符集占用4个字节 , 能还原
     * <p>
     * 通过getBytes的方式获得指定字符集对应的字节 : z1.getBytes("UNICODE")
     * 通过字节数组 + 字符集 组成字符: new String(${bytes},Charset.forName("UNICODE"))
     *
     * @throws UnsupportedEncodingException
     */
    private static void printLength(String z1) throws UnsupportedEncodingException {
        System.out.println("str: " + z1 + " length: " + z1.getBytes("ISO-8859-1").length + " encoding: " + "ISO-8859-1");
        System.out.println("str: " + z1 + " length: " + z1.getBytes("GBK").length + " encoding: " + "GBK");
        System.out.println("str: " + z1 + " length: " + z1.getBytes("UTF-8").length + " encoding: " + "UTF-8");
        System.out.println("str: " + z1 + " length: " + z1.getBytes("UNICODE").length + " encoding: " + "UNICODE");
    }
}