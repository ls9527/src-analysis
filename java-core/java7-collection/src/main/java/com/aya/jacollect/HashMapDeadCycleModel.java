package com.aya.jacollect;


import java.io.Serializable;

public class HashMapDeadCycleModel implements Serializable {
    private int hash;
    private String equalStr;

    public HashMapDeadCycleModel(int hash, String equalStr) {
        this.hash = hash;
        this.equalStr = equalStr;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    public String getEqualStr() {
        return equalStr;
    }

    public void setEqualStr(String equalStr) {
        this.equalStr = equalStr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HashMapDeadCycleModel hashMapDeadCycleModel = (HashMapDeadCycleModel) o;
        return equalStr.equals(hashMapDeadCycleModel.getEqualStr());
    }

    @Override
    public int hashCode() {
        return hash;
    }
}
