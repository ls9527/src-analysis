package com.aya.jacollect.test;

import com.aya.jacollect.HashMapDeadCycleModel;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;

/**
 * JAVA7 的HashMap死循环， 是在多线程环境下的hash碰撞节点产生了环形连接.
 * 产生了环形链表之后的 put,get,remove 会引起死循环。
 * 环形链表产生的过程中不会死循环
 *
 * 以oracle jdk1.7.0_80 为例子.
 * 需要在两处打断点:
 *
 *  void transfer(Entry[] newTable, boolean rehash) {
 *         int newCapacity = newTable.length;
 *         for (Entry<K,V> e : table) {
 *             while(null != e) {
 *                 Entry<K,V> next = e.next;
 *                 if (rehash) { // 线程A在if判断处打断点
 *                     e.hash = null == e.key ? 0 : hash(e.key);
 *                 }
 *                 int i = indexFor(e.hash, newCapacity);
 *                 e.next = newTable[i];
 *                 newTable[i] = e;
 *                 e = next;
 *             }
 *         }
 *     }
 *
 *
 *     void resize(int newCapacity) {
 *         Entry[] oldTable = table;
 *         int oldCapacity = oldTable.length;
 *         if (oldCapacity == MAXIMUM_CAPACITY) {
 *             threshold = Integer.MAX_VALUE;
 *             return;
 *         }
 *
 *         Entry[] newTable = new Entry[newCapacity];
 *         transfer(newTable, initHashSeedAsNeeded(newCapacity));
 *         table = newTable;
 *         // 线程B在给 table 赋值之后打断点
 *         threshold = (int)Math.min(newCapacity * loadFactor, MAXIMUM_CAPACITY + 1);
 *     }
 *
 */
public class HashMapDeadCycleTest {

    @Test
    public void testGetKey() throws Exception {

        final HashMap<HashMapDeadCycleModel, String> cycleMap = getCycleMap();
        // 获取不存在的key, 引起死循环
        cycleMap.get(new HashMapDeadCycleModel(1, "NotExistsKey"));

    }

    @Test
    public void testPutKey() throws Exception {
        final HashMap<HashMapDeadCycleModel, String> cycleMap = getCycleMap();
        // 再次插入hash冲突的key, 引起死循环
        cycleMap.put(new HashMapDeadCycleModel(1, "NewKey"),"NewKey-VALUE");
    }

    @Test
    public void testRemoveKey() throws Exception {
        final HashMap<HashMapDeadCycleModel, String> cycleMap = getCycleMap();
        // 移除hash冲突的键位, 不存在的key
        cycleMap.remove(new HashMapDeadCycleModel(1, "KeyDoesNotEquals"));
    }

    private static HashMap<HashMapDeadCycleModel, String> getCycleMap() throws Exception {
        while (true) {


            final HashMap<HashMapDeadCycleModel, String> stringHashMap = new HashMap<>(4);
            // 由于JDK7的头插特性, 所以链表为:  A--> B --> C
            stringHashMap.put(new HashMapDeadCycleModel(1, "C"), "C-VALUE");
            stringHashMap.put(new HashMapDeadCycleModel(1, "B"), "B-VALUE");
            stringHashMap.put(new HashMapDeadCycleModel(1, "A"), "A-VALUE");
            final Field tableField = HashMap.class.getDeclaredField("table");
            tableField.setAccessible(true);
            final Object o = tableField.get(stringHashMap);
            // ((HashMap.Entry[])o)[1] = A-VALUE
            // ((HashMap.Entry[])o)[1].next = B-VALUE
            // ((HashMap.Entry[])o)[1].next.next = C-VALUE

            final CountDownLatch countDownLatch = new CountDownLatch(1);
            Thread thread1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        countDownLatch.await();
                        stringHashMap.put(new HashMapDeadCycleModel(1, "ThreadA"), "ThreadA-VALUE");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }, "ThreadA");

            Thread thread2 = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        countDownLatch.await();
                        stringHashMap.put(new HashMapDeadCycleModel(1, "ThreadB"), "ThreadB-VALUE");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }, "ThreadB");
            thread1.start();
            thread2.start();
            countDownLatch.countDown();


            thread1.join();
            thread2.join();
            // 如果产生了环形队列, 则可以退出
            if (hasCycle(stringHashMap)) {
                return stringHashMap;
            }
        }
    }

    //    1 = {HashMap$Entry@875}
    //        key = {HashMapDeadCycleModel@876}
//        value = "ThreadB-VALUE"
//        next = {HashMap$Entry@878}
//        key = {HashMapDeadCycleModel@879}
//        value = "A-VALUE"
//        next = {HashMap$Entry@862}
//        key = {HashMapDeadCycleModel@863}
//        value = "B-VALUE"
//        next = {HashMap$Entry@881}
//        key = {HashMapDeadCycleModel@882}
//        value = "C-VALUE"
//        next = {HashMap$Entry@862}
//        key = {HashMapDeadCycleModel@863}
//        value = "B-VALUE"
//        next = {HashMap$Entry@881}
//        key = {HashMapDeadCycleModel@882}
//        value = "C-VALUE"
//        next = {HashMap$Entry@862}
    private static boolean hasCycle(HashMap<HashMapDeadCycleModel, String> stringHashMap) throws NoSuchFieldException, IllegalAccessException {

        final Field table = HashMap.class.getDeclaredField("table");
        table.setAccessible(true);
        final Object[] o = (Object[]) table.get(stringHashMap);
        final Object entry1 = o[1];
        final Field next = entry1.getClass().getDeclaredField("next");
        next.setAccessible(true);
        final Field value = entry1.getClass().getDeclaredField("value");
        value.setAccessible(true);
        final Object entry2 = next.get(entry1);
        final Object entry3 = next.get(entry2);

        if (entry2 != null && "A-VALUE".equals(value.get(entry2))) {
            if (entry3 != null && "B-VALUE".equals(value.get(entry3))) {
                final Object entry4 = next.get(entry3);
                if (entry4 != null && "A-VALUE".equals(value.get(entry4))) {
                    return true;
                }
            }
        }

        if (entry3 != null && "A-VALUE".equals(value.get(entry3))) {
            final Object entry4 = next.get(entry3);
            if (entry4 != null && "B-VALUE".equals(value.get(entry4))) {
                final Object entry5 = next.get(entry4);
                if (entry5 != null && "A-VALUE".equals(value.get(entry5))) {
                    return true;
                }
            }
        }

        return false;
    }
}
