package com.aya;

import org.junit.Test;
import org.mockito.exceptions.verification.WantedButNotInvoked;

import static org.mockito.Mockito.*;

public class LoginControllerTest {
    @Test
    public void testSimple() {
        // 构建mock对象
        LoginController loginController = mock(LoginController.class);
        // 修改mock对象的返回值
        when(loginController.login()).thenReturn("s-test");
        // 打印mock对象调用方法的返回值
        System.out.println(loginController.login());
    }

    @Test(expected = WantedButNotInvoked.class)
    public void testVerifyFail() {
        LoginController loginController = mock(LoginController.class);
        when(loginController.login()).thenReturn("s-test");
        verify(loginController).login();
    }
    // 验证mock对象的某个方法必须要被调用
    @Test
    public void testVerifySuccess() {
        LoginController loginController = mock(LoginController.class);
        String loginResult = loginController.login();
        System.out.println(loginResult);
        verify(loginController).login();
    }

}
